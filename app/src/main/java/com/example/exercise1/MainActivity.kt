package com.example.exercise1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var textArea:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textArea=findViewById(R.id.textArea)
        textArea.text="Press one of the buttons"
    }

    fun btnClick1(v:View){
        var textoArea:String=textArea.text.toString()
        textArea=findViewById(R.id.textArea)
        textArea.setText("You have press the button of Text 1")

    }

    fun btnClick2(v:View){
        var textoArea:String=textArea.text.toString()
        textArea=findViewById(R.id.textArea)
        textArea.setText("You have press the button of Text 2")

    }
}